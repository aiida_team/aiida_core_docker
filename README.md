# Overview

It is a base AiiDA installation dockerfile. It installs the aiida_core 
from (https://github.com/aiidateam/aiida_core) with default values.


# Quick Reference


- Install/Update docker as described here: https://docs.docker.com/engine/installation/linux/ubuntulinux/

- Clone the docker git repository as:
```bash
git clone git@bitbucket.org/aiida_team/aiida_core_docker.git
```

- Modify installation settings in aiida_docker/aiida.config. Default installation configuration is:
```bash
django
Europe/Zurich
your_email@epfl.ch
postgresql_psycopg2
localhost
5432
aiidadb
aiida
aiida
~/.aiida/repository-default/
YourFirstName
YourLastName
EPFL
N
```

Above configuration file is list of answers for the AiiDA installation step i.e. *verdi install* described [here](http://aiida-core.readthedocs.io/en/latest/installation.html#aiida-first-setup).
Note that the password can't be set from config file and hence always set *N* for following question:
```bash
The user has no password, do you want to set one? [y/N] N
```


- That's all! You can now build aiida docker image as:

```bash
docker build --build-arg username=yourusername --build-arg password=yourpassword -t aiida .
```

 > Note:  The username and password is used for creating an account on linux container.

- Now you can start container as:
```bash
docker run -i -t --entrypoint /bin/bash aiida
```

# Common Docker commands

- To build image from Dockerfile (username and password are passed as an arguments to the build command)
```bash
docker build --build-arg username=yourusername --build-arg password=yourpassword -t aiida .
```

- To get list of built docker images
```bash
docker images
```

- To start the container from built docker image (e.g. image name = aiida). 
```bash
docker run -i -t --entrypoint /bin/bash aiida
```
  > Note: You can start multiple containers from same image independently by running above command multiple times. 


- To close the running container, just run regular linux "exit" command from the container.


- To get list of running containers use
```bash
docker ps
```

- From "docker ps" command, you get the container ID. You can connect/log-in into already running container from different terminal using command:
```bash
docker exec -i -t 8a31297e46b6 bash               # container id: 8a31297e46b6
```

- To stop all running containers from host machine use:
```bash
docker stop $(docker ps -a -q)
```

- To stop a running container from host machine use:
```bash
docker stop 8a31297e46b6                          # container id: 8a31297e46b6
```

- To delete all containers from host machine use:
```bash
docker rm $(docker ps -a -q)
```

- To delete a container from host machine use:
```bash
docker rm 8a31297e46b6                            # container id: 8a31297e46b6
```

- to delete all images from host machine use:
```bash
docker rmi $(docker images -q)
```

- To delete a image from host machine use:
```bash
docker rmi aiida                                  # image name: aiida
```