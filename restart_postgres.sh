wait_count=0

echo ""
echo "NOTE: Some containers take time to start"
echo "Postgres server. This script waits for server"
echo "to start. If this is taking "
echo "more than few minutes, then this"
echo "is likely an error in installation!"
echo ""

sudo -u postgres psql -c '\q' 2>/dev/null

while [[ "$?" != "0" ]]; do
    echo -ne "=> Waiting for PostgresSql to start : $wait_count Seconds"\\r
    sleep 1
    wait_count=$((wait_count+1))
    sudo -u postgres psql -c '\q' 2>/dev/null
done


