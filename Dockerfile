# Automated deployment script for AiiDA CORE version
# MAREVL, EPFL

FROM ubuntu:16.04
MAINTAINER Snehal Waychal <snehal.waychal@epfl.ch>

# set the following environment so that no question/dialog is asked during apt-get install
ENV DEBIAN_FRONTEND noninteractive

##########################################
############ Base Image Setup ############
##########################################

RUN apt-get update \
    && apt-get -y install software-properties-common \
    && apt-get -y --force-yes install \
    build-essential \
    git \
    openssh-client \
    sudo \
    postgresql-9.5 \
    postgresql-server-dev-9.5 \
    postgresql-client-9.5 \
    python-pip \
    ipython \
    python2.7-dev \
    jmol \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get clean all \
    && pip install -U pip


##########################################
############ Postgres Setup ##############
##########################################

# new database for AiiDA with PostgreSQL
USER postgres

# aiida database
RUN /etc/init.d/postgresql start && \
    psql --command "CREATE USER aiida WITH PASSWORD 'aiida';" && \
    psql --command "CREATE DATABASE aiidadb OWNER aiida;" && \
    psql --command "GRANT ALL PRIVILEGES ON DATABASE aiidadb to aiida;"


##########################################
############### User Setup ###############
# NOTE: Here we are creating 'aiida' a sudo
# user with password 'aiida' to install
# AiiDA CORE version. In future, we might
# remove it and install AiiDA with root user
##########################################

# create sudo user
USER root

# default username and password, otherwise use command line arguments
ARG username=aiida
ARG password=aiida

ENV AIIDA_USER $username
ENV AIIDA_PASS $password

# add USER with sudo permissions
RUN useradd -m -s /bin/bash ${AIIDA_USER} \
    && echo "${AIIDA_USER}:${AIIDA_PASS}" | chpasswd \
    && adduser ${AIIDA_USER} sudo \
    && echo ${AIIDA_USER}' ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers

# install rest of the packages as normal user
USER $AIIDA_USER

# set $HOME, create git directory
ENV HOME /home/$AIIDA_USER
RUN mkdir -p $HOME/git/
RUN mkdir -p $HOME/docker_scripts/


##########################################
############ Installation Setup ##########
##########################################

# clone repo and all config files
WORKDIR $HOME/git/
RUN git clone https://github.com/aiidateam/aiida_core.git
COPY aiida.config $HOME/docker_scripts/aiida.config
COPY restart_postgres.sh $HOME/docker_scripts/restart_postgres.sh
RUN sudo chmod +x $HOME/docker_scripts/restart_postgres.sh

WORKDIR $HOME/git/aiida_core
RUN git checkout develop


USER root
# This is required for some recent features of setup.py
RUN pip install -U pip wheel setuptools

# Install AiiDA with some optional dependencies
RUN pip install -U .[verdi_shell,ssh,REST] --process-dependency-links

USER $AIIDA_USER

# RUN pip install --user -U -r requirements.txt

# add  PATH and PYTHONPATH configuration
env PYTHONPATH $HOME/git/aiida_core:$PYTHONPATH
env PATH $HOME/git/aiida_core/bin:$PATH

# verdi auto-complete
RUN verdi completioncommand >> $HOME/.bashrc

# start AiiDA installation
RUN bash -c "sudo service postgresql restart && sleep 5 && $HOME/docker_scripts/restart_postgres.sh && verdi install < $HOME/docker_scripts/aiida.config"


# Set the locale for utf-8 encoding
RUN sudo locale-gen en_US.UTF-8  
ENV LANG en_US.UTF-8  
ENV LANGUAGE en_US:en  
ENV LC_ALL en_US.UTF-8

# restart services again
COPY start_all.sh $HOME/docker_scripts/start_all.sh
RUN sudo chmod +x $HOME/docker_scripts/start_all.sh
CMD $HOME/docker_scripts/start_all.sh
